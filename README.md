Facebook Album Downloader JavaScript v.1.1
==========================================

Dieses Skript läd alle Bilder von einem Facebook Foto Album herunter.
Mit einem Click.

Idee
----
Sie klicken auf ein Lesezeichen das Javascript beinhaltet. Diese fängt an
alle Bilder von der gerade aufgeschlagene Facebook **Album Seite** zu Downloaden.

>WICHTIG: Es muss die Album Seite sein wo alle Bilder in kleine nacheinander auf gezeigt sind. 
Nicht die in der Pinnwand oder die vergrößerte der Diashow. (Dort funktioniert es nicht)*

Installation
------------
Sie legen ein Lesezeichen in ihrem Browser an. Am besten in der Lesezeichen-Symbolleist, die über jede Webseite ist.
Dort fügen sie als Adresse dieses JavaScript ein:

``
javascript:(function(){var%20e=document.createElement("div"),c=document.getElementsByTagName("body")[0];otherlib=false,msg="";count=null;version="1.1";e.style.position="fixed";e.style.height="32px";e.style.width="220px";e.style.marginLeft="-110px";e.style.top="0";e.style.left="50%";e.style.padding="5px%2010px";e.style.zIndex=1001;e.style.fontSize="12px";e.style.color="#222";e.style.backgroundColor="#f99";function%20d(){var%20g=0;var%20b=jQuery('a[ajaxify*="n.jpg"]').each(function(j,h){g=g+1;var%20k=g;var%20i=(unescape((jQuery(h).attr("ajaxify").split("src="))[1]).split("&"))[0]+"?dl=1";window.setTimeout(function(){window.document.location=i;e.innerHTML="Download%20Picture%20"+k+"%20of%20"+count},g*3000)});msg="Facebook%20Picture%20Downloader%20v"+version;count=b.length;return%20f(g)}if(typeof%20jQuery!="undefined"){return%20d()}else{if(typeof%20$=="function"){otherlib=true}}function%20a(h,j){var%20g=document.createElement("script");g.src=h;var%20i=document.getElementsByTagName("head")[0],b=false;g.onload=g.onreadystatechange=function(){if(!b&&(!this.readyState||this.readyState=="loaded"||this.readyState=="complete")){b=true;j();g.onload=g.onreadystatechange=null;i.removeChild(g)}};i.appendChild(g)}a("http://code.jquery.com/jquery-latest.min.js",function(){d()});function%20f(b){e.innerHTML=msg;c.appendChild(e);window.setTimeout(function(){if(typeof%20jQuery=="undefined"){c.removeChild(e)}else{jQuery(e).fadeOut("slow",function(){jQuery(this).remove()});if(otherlib){$jq=jQuery.noConflict()}}},10000+b*3000)}})();
``

Anleitung
---------
Sie gehen auf eine Album Seite wo **alle** Bilder **in klein** Dargestellt sind.
Dort klicken Sie nun auf das Lesezeichen in Ihrem Browser. 
Das Script wird nun anfangen Automatisch die Bilder herunterzuladen.

Hintergrund wissen / Funktionweise
----------------------------------
Es wird erstmal jQuery geladen. Danach werden Links gesucht die auf *n.jpg enden.
Nachdem alle gefunden sind. Werden Sie heruntergeladen.


VIEL SPASS
==========
**Tobi**

Bei fragen können Sie mir gerne eine Email schicken: info (AT) tobi-mat . eu

*PS:Hoffentlich ändert Facebook nicht html-code*