javascript: (function() {
	//Erstell das Msg Div
	var el = document.createElement('div'),
	b = document.getElementsByTagName('body')[0];
	otherlib = false,
	msg = '';
	count = null;
	version = "1.1"
	el.style.position = 'fixed';
	el.style.height = '32px';
	el.style.width = '220px';
	el.style.marginLeft = '-110px';
	el.style.top = '0';
	el.style.left = '50%';
	el.style.padding = '5px 10px';
	el.style.zIndex = 1001;
	el.style.fontSize = '12px';
	el.style.color = '#222';
	el.style.backgroundColor = '#f99';
	//hier ist der Downloader der soll zu erst geparst werden
	function startDL() {
		//i wird gebraucht um setTimeout zeitversetzt aus zu lösen. alles auf einmal geht nicht zu laden.
		var i = 0; 
		/*
		 * Facebook Link sieht so aus: 
		 <a ajaxify="http://www.facebook.com/photo.php?fbid=10150538612108966&amp;set=a.10150538611948966.395905.789378965&amp;type=3&amp;
		 src=http%3A%2F%2Fa2.sphotos.ak.fbcdn.net%2Fhphotos-ak-snc7%2F400439_10150538612108966_789378965_8805132_779006175_n.jpg&amp;theater&amp;size=640%2C425"></a>

		 - [1] alle Anker suchen die im ajaxify-Attribute ein n.jpg beinhalten. Das sind die Groß Bilder
		 - [2] String teilen und alles nehmen was nach src= ist
		 - [3] zusatzt Parameter entfernen
		 - [4] Download Paramter dazuhängen
		 - [5] setTimeout setzten zu einen Download wenn es drann ist.
		*/
		var pic=jQuery('a[ajaxify*="n.jpg"]') // [1]
			.each(function(e, a) {
				i = i + 1; var c=i;
				var bild = (unescape((jQuery(a).attr('ajaxify').split("src="))[1]) //[2]
					.split("&"))[0] //[3]
					+ "?dl=1"; //[4]
					window.setTimeout(function() { //[5]
						window.document.location = bild;
						el.innerHTML = 'Download Picture '+c+" of "+count;
					}, i * 3000) //All 3sec spaeter soll der Download Anfangen
			});
		msg = 'Facebook Picture Downloader v'+version;
		count = pic.length;
		return showMsg(i);
	}
	//Verhindert ein doppeltes laden von jQuery
	if (typeof jQuery != 'undefined') {
		return startDL();
	} else if (typeof $ == 'function') {
		otherlib = true;
	}
	//Läd die jQuery-Libery, Wenn fertig startet den Download.
	function getScript(url, success) {
		var script = document.createElement('script');
		script.src = url;
		var head = document.getElementsByTagName('head')[0],
		done = false;
		script.onload = script.onreadystatechange = function() {
				if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
					done = true;
					success();
					script.onload = script.onreadystatechange = null;
					head.removeChild(script);
			}
		};
		head.appendChild(script);
	}
	getScript('http://code.jquery.com/jquery-latest.min.js', function(){startDL()});
	function showMsg(time) {
		el.innerHTML = msg;
		b.appendChild(el);
		window.setTimeout(function() {
			if (typeof jQuery == 'undefined') {
				b.removeChild(el);
			} else {
				jQuery(el).fadeOut('slow', function() {
					jQuery(this).remove();
				});
				if (otherlib) {
					$jq = jQuery.noConflict();
				}
			}
		},
		10000+time*3000);
	}
})();